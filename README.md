# Docker-Command

## PgAdmin + Postgresql Docker

1; Run a postgres container:

`docker run --name postgres-db -p 5432:5432 -v postgrest-data:/var/lib/postgresql/data -e POSTGRES_PASSWORD=imallstar09 -e POSTGRES_DB=dbtest -d -it postgres`

`docker exec -it postgres-db bash`

`psql -h localhost -p 5432 -U postgres`

`CREATE ROLE username WITH CREATEDB LOGIN PASSWORD 'password';`

`CREATE DATABASE dbname OWNER username;`

2; Run pgAdmin container:

`docker run --name pg-admin4 -p 8000:80 --link postgres-db -e "PGADMIN_DEFAULT_EMAIL=allstar.perfect@gmail.com" -e "PGADMIN_DEFAULT_PASSWORD=imallstar09" -d -it dpage/pgadmin4`

## Elastic search (ASP.NET Core)

`https://miroslavpopovic.com/posts/2018/07/elasticsearch-with-aspnet-core-and-docker`

pull docker

`docker pull docker.elastic.co/elasticsearch/elasticsearch:6.3.0`
`docker pull docker.elastic.co/kibana/kibana:6.3.0`

run docker

`docker network create esnetwork --driver=bridge`
`docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elasticsearch -d --network esnetwork docker.elastic.co/elasticsearch/elasticsearch:6.3.0`
`docker run -p 5601:5601 --name kibana -d --network esnetwork docker.elastic.co/kibana/kibana:6.3.0`

## Oracle 12c

### On Run

docker run -d -it --name oracle -p 1521:1521 -p 5500:5500 \
-e ORACLE_SID=xe \
-e ORACLE_PDB=ETCWMD \
-e ORACLE_PWD=Dft@2018 \
-e ORACLE_CHARACTERSET=AL32UTF8 \
-v /oracle12c/data:/u01/app/oracle/oradata store/oracle/database-enterprise:12.2.0.1

docker run -d --name oracle-se \
  --shm-size=1g \
  -p 1521:1521 \
  -e ORACLE_SID=xe \
  -e ORACLE_PWD=Dft@2018 \
  -e ORACLE_PDB=ETCWMD \
  -e ORACLE_CHARACTERSET=AL32UTF8 \
  -v /oracle12c/tcwmd-data:/u01/app/oracle/oradata daggerok/oracle:12.1.0.2-se2

### On Shell

docker exec -it oracle bash -c "source /home/oracle/.bashrc; sqlplus /nolog"
docker exec -it oracle sh
CONNECT SYS AS SYSDBA // no password
CONNECT / AS SYSDBA // no password

GRANT CREATE SESSION TO ETCWMD;
GRANT CREATE TABLE TO ETCWMD;
GRANT CREATE VIEW TO ETCWMD;

### On Drop All

SELECT 'DROP TABLE "' || TABLE_NAME || '" CASCADE CONSTRAINTS;' FROM user_tables;
SELECT 'DROP SEQUENCE ' || SEQUENCE_NAME || ';' FROM user_sequences;

## MongoDB

`docker run --name mongodb -v /mongoDB/data:/data/db -p 27017:27017 -d mongo:3.2.12 --auth`

`docker exec -it tcwmd-mongodb mongo admin`

db.createUser({
user: "admin",
pwd: "Dft_2018",
roles: [ { role: "root", db: "admin" } ]
})

### Build & Push tage docker on Linux

`docker build . -t <docker-tag>`
`docker push <docker-tag>`

## MQTT Server


`docker run -ti -d --name "mqtt-broker" -p 1883:1883 -p 9001:9001 toke/mosquitto`

## MySQL + Php MyAdmin

MySQL Server

`docker run -d -it --name mysql-server -v /mysql/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw mysql`

PHP MyAdmin

`docker run -d -it --name php-myadmin --link mysql-server:db -p 8080:80 phpmyadmin/phpmyadmin`



